## README (EN)
**pySiriL** ( **py**thon **SiriL**) is a python library to interface python to a SiriL script.
### Overview
a brief overview of pysril is available here : [https://siril.org/tutorials/pysiril/](https://siril.org/tutorials/pysiril/)
### Examples
in the "pysiril/example" folder there are some usage examples. 
### Installation
Two ways to do it:
1. from the sources:
    - download or clone the git repository : https://gitlab.com/free-astro/pysiril
    - if necessary, install modules : setuptools and wheel
        * python -m pip install setuptools
        * python -m pip install wheel
    - create the pysiril package : 
        * python setup.py bdist_wheel
    - if necessary, uninstall the old pysiril package:
        * python -m pip uninstall pysiril
    - install the package:
        * cd dist
        * python -m pip install pysiril-0.0.x-py3-none-any.whl
2. from a whl package
    - download the latest release: https://gitlab.com/free-astro/pysiril/-/releases
    - then type in a console:
        * python -m pip install pysiril-x.y.z-py3-none-any.whl

## LISEZ-MOI (FR)
**pySiriL** ( **py**thon **SiriL**) est une librairie python pour interfacer python au langage de script de SiriL.
### Introduction
un bref aperçu est disponible ici : [https://siril.org/fr/tutorials/pysiril/](https://siril.org/fr/tutorials/pysiril/)
### Exemples
dans le dossier "pysiril/example", il y a quelques exemples d'utilisation.
### Installation
Deux façon de procéder:
1. à partir des sources:
    - telechargez ou clonez le depot git: https://gitlab.com/free-astro/pysiril
    - si necessaire, installer des modules: setuptools et wheel
      * python -m pip install setuptools
      * python -m pip install wheel
    - creez le package pysiril:
        * python setup.py bdist_wheel
    - si necessaire, desinstallez l'ancien package pysiril:
        * python -m pip uninstall pysiril
    - installez le package:
        * cd dist
        * python -m pip install pysiril-0.0.x-py3-none-any.whl
2. à partir d'un paquet whl
    - télécharger la dernière release : https://gitlab.com/free-astro/pysiril/-/releases
    - puis taper dans  une console:
        * python -m pip install pysiril-x.y.z-py3-none-any.whl


