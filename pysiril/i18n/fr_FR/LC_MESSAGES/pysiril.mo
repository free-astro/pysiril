��    /      �  C                   %  	   -  0   7  /   h     �     �  "   �     �     �               (     -     <      R  $   s     �     �      �  )   �     �  3        :     B     J     b     n  
   �  
   �  
   �     �     �     �     �     �     �            +   :     f     �     �     �  	   �     �    �     �     �     �  1   �  3   	     L	     X	  %   ^	  $   �	     �	     �	     �	     �	     �	     	
  "   $
  )   G
     q
     y
  (   �
  -   �
     �
  K   �
     A  
   I     T     l     }     �     �     �     �     �     �     �     �            '   5  1   ]     �     �     �     �  
   �     �           ,       )                   +           #   &       !      .          $       /         '   
      -                           "             *             (   	                                                        %              ... aborted Aborted Aborted : Are you sure you want to overwrite image files?  Check if the "Developer Mode" option is enabled Cleaning :  Copy Error import win32pipe & win32file Error, python version should be Error: Siril don't work First step: Starting Initialisation Link Python version Second step: Starting Siril is compatible with pysiril Siril is not compatible with pySiril Skip Stopping Symbolic Link Error on Windows:  The module pywin32 should be downloaded : Third step: Starting Timeout expired : Problem to run 'Siril --version'  VERSION aborted directory doesn't exist don't exist incoherent extension is aborted is started is stopped no not processed or pySiril requires : Siril  pysiril is already closed pysiril is already opened pysiril is not ready pysiril is not ready or closed pysiril needs the path  of executable siril pysiril uses by default : run siril don't exist skipping waiting:  yes Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2021-11-23 17:49+0100
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
Plural-Forms: nplurals=2; plural=(n > 1);
 … Annuler Echec Echec: Etes-vous sûr de vouloir d'écraser les images?  Vérifiez si l'option "Developer Mode" est activée Nettoyage:  Copie Erreur "import win32pipe & win32file" Erreur, la version python doit être Erreur: SIRIL ne fonctionne pas Première étape: Démarrage Initialisation Lien Version Python Seconde étape: Démarrage Siril est  compatible avec pySiril Siril n’est pas compatible avec pySiril Ignorer Arrêt Erreur de lien symbolique sur Windows :  Le module "pywin32" doit être téléchargé: Troisième étape: Démarrage Délai d’attente expiré : Problème d’exécution de 'Siril --version'  VERSION abandonné le dossier n'existe pas n’existent pas extension incohérente est abandonné est démarré est arrêté non pas traité ou pySiril exige: Siril  pysiril est déjà fermé pysiril est déjà ouvert pysiril n’est pas prêt pysiril n’est pas prêt ou est fermé pysiril a besoin du chemin de l'exécutable SiriL pysiril utilise par défaut: run siril n'existe pas passe au suivant attendre:  oui 