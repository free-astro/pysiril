# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: pysiril ( Python SiriL )
#
# This is a library to interface python to Siril.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import sys
import os
import subprocess
import threading

import LogMsg
import traceback
import locale

# ==============================================================================

class ThreadSiril(threading.Thread):
    def __init__(self, trace, siril_exe ):
        self.siril_exe    = siril_exe
        self.tr           = trace
        self.runningSiril = False
        threading.Thread.__init__(self)

    def run(self):
        self.tr.info("ThreadSiril "+ _("is started"))
        self.runningSiril = True
        cmd_siril=[self.siril_exe,"-p"]
        self.tr.info(_("run") + " : " + " ".join(cmd_siril))
        try :
            if os.getenv('LANG') is  None :
                os.environ['LANG']='C'
            #memo_lang=os.environ['LANG']
            #os.environ['LANG']='C'
            psiril = subprocess.Popen(cmd_siril,stderr=subprocess.STDOUT, stdout = subprocess.PIPE)
            self.Siril_output = (psiril.communicate()[0]).decode(locale.getpreferredencoding(),'ignore')
            #os.environ['LANG']=memo_lang
        except Exception as e :
            self.Siril_output = _("Aborted :") + cmd_siril[0] + "\n"
            self.tr.error("*** ThreadSiril::run() " + str(e)+"\n")
            self.tr.error("*** " + traceback.format_exc() +"\n")

        self.runningSiril = False
        self.tr.info("ThreadSiril "+ _("is stopped"))

    def IsRunning(self):
        return self.runningSiril

    def GetOutput(self):
        return self.Siril_output

