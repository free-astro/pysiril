# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: pysiril ( Python SiriL )
#
# This is a library to interface python to Siril.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------
NO_VERSION="0.0.17"
CHANGELOG="CHANGELOG: \n"
CHANGELOG= CHANGELOG + "V" + NO_VERSION + """
    + update seq writing 
    + update stats reading
    + fix: missing line in WriteSeqFile (upscale_at_stacking was forgotten)
    + fix: path with space characters
V0.0.16
    + fix Update addons::GetSeqFile
    + add Addons::WriteSeqFile method
    + added a trace to facilitate debugging
    + fix: preprocess and preprocess_single are deprecated
    + fix: 
V0.0.15
    + fix: linux or macos bug
    + fix: stat command fields
    + fix: filtering options
V0.0.14
    + add : autoghs, calibrate, calibrate_single, catsearch, getref,
            platesolve, sb, seqght, seqplatesolve, seqsb, seqstarnet,
            seqwiener, unselect, wiener, seqlinstretch, seqmodasinh
            
    + fix : binxy, denoise, ght, invght , modasinh, pcc, preprocess
            preprocess_single, rmgreen, satu, seqrl, setfindstar,
            split
V0.0.13
    + update -> COMPATIBILITY  SIRIL 1.2.0
    
V0.0.12
    + update -> COMPATIBILITY_SIRIL siril 1.0.0
    + accelerate start-up phase
V0.0.11
    + update -> siril 1.0.0-RC1
    + fix if os.environ['LANG'] == None
    + fix remove_file() with symbolic link when the source file is deleted
V0.0.10
    + update for siril 0.99.10:
        o stack & stackall : rejection type , weighted
        o extract_Green
        o seqextract_Green
V0.0.9
    + fix conditional package for setup.py
    + fix remove_file() if read only
V0.0.8
   + remove 1s standby for siril>=0.99.8
   + add requires 
   + minor fix in Addons::ReadFITSHeader
V0.0.7
   + fix SiriL version check
   + fix ReadFITSHeader
   + fix SetRef()
V0.0.6\n"
   + Add Addons::ReadFITSHeader()
   + Add Addons::GetSirilPrefs()
   + add Addons::seqstat()\n"
   + add Addons::GetSeqType(() and  Addons::GetSeqFile()
   + fix minor  in wrapper->preprocess
V0.0.5
   + add 'requires' command
V0.0.4
   + update docstring of wrapper
   + update french translation
   + check the end of command
V0.0.3
   + fix help()
V0.0.2
   + fix README.md
   + fix error ( by Cissou8 )
   + fix check version
   + update docstring wrapper functions
   + add french traduction
 V0.0.1
   + initial version
"""
