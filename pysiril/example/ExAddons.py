# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: pysiril ( Python SiriL )
#
# This is a library to interface python to Siril.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import sys
import os

try:
    from pysiril.siril   import *
    from pysiril.wrapper import *
    from pysiril.addons  import *
except:
    print(" ***  Mode Developement - la lib pysiril n'est pas installe *** ")
    path_exec=os.path.dirname(  __file__ )
    if not path_exec :
        path_exec='.'
    sys.path.append(path_exec + os.sep + '..')
    from siril   import *
    from wrapper import *
    from addons  import *

# ==============================================================================
# Example of functions which return values
# ==============================================================================

print("StatusFunctions:begin")
app=Siril()
try:
    cmd=Wrapper(app)
    fct=Addons(app)
    
    
    # Create a seqfile
    
    workdir     = "D:/_TraitAstro/20-SiriL/work/TestSiril"
    processdir  = workdir + "/" + "xxxx"
    fct.CleanFolder(processdir,ext_list=[ ".cr2",".seq"])
    fct.MkDirs(processdir)
    NbImage= fct.GetNbFiles(workdir+ '/lights/*.CR2')
    print( "CR2 number:",NbImage)
    
    number=fct.NumberImages(workdir+ '/lights/*.CR2',processdir,"offsets",start=10,bOverwrite=True)
    if number == NbImage  :
        fct.CreateSeqFile(processdir+"/toto.seq", number )
    else:
        print("error of images number:",number, "<>",NbImage)
except Exception as e :
    print("\n**** ERROR *** " +  str(e) + "\n" )    
    
app.Close()
del app

print("StatusFunctions:end")
       