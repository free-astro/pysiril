import sys
import os


try:
    from pysiril.siril   import *
    from pysiril.wrapper import *
    from pysiril.addons  import *
except:
    print(" ***  Mode Developement - la lib pysiril n'est pas installe *** ")
    path_exec=os.path.dirname(  __file__ )
    if not path_exec :
        path_exec='.'
    sys.path.append(path_exec + os.sep + '..')
    from siril   import *
    from wrapper import *
    from addons  import *

app=Siril()
#app=Siril("C:/awin/msys64/mingw64/bin/siril.exe", True)
app.Open()
cmd=Wrapper(app)

help( Siril )               # pour la doc des fonctions de base de la librairie
help( Wrapper )             # pour l'aide des fonctions wrapper *** pas fini ***
help( Addons )              # pour l'aide des fonctions annexes

cmd.help()                  # pour l'aide de toutes les fonctions SiriL
cmd.help( 'convertraw')     # pour l'aide d'une fonction Siril
help(cmd.convertraw)        # pour l'aide wrapper d'une fonction Siril

app.Close()
del app
