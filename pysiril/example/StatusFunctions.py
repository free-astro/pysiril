# -*- coding: UTF-8 -*-
# ==============================================================================

# ------------------------------------------------------------------------------
# Project: pysiril ( Python SiriL )
#
# This is a library to interface python to Siril.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import sys
import os

try:
    from pysiril.siril   import *
    from pysiril.wrapper import *
    from pysiril.addons  import *
except:
    print(" ***  Mode Developement - la lib pysiril n'est pas installe *** ")
    path_exec=os.path.dirname(  __file__ )
    if not path_exec :
        path_exec='.'
    sys.path.append(path_exec + os.sep + '..')
    from siril   import *
    from wrapper import *
    from addons  import *

# ==============================================================================
# Example of functions which return values
# ==============================================================================

print("StatusFunctions:begin")
app=Siril()
try:
    cmd=Wrapper(app)
    fct=Addons(app)
    #app.Display(bLog=True,bInfo=False)
    #app.MuteSiril(True)
    app.Open()
    
    if sys.platform.startswith('win32'): 
        app.Execute("cd 'D:/_TraitAstro/20-SiriL/work/M101/Temp'")
        app.Execute("load C0")
        app.Execute("load 'D:/_TraitAstro/20-SiriL/work/M101/M101_c.fit'")
    else:
         app.Execute("cd '/home/barch/siril/work'")
         app.Execute("load M31_DSLR")
    
    if False :
        cmd.help()
        cmd.help("stat")
        cmd.help("xxx")
    
    #app.Execute("bg")
    #print(app.GetData())
    print("BG:" ,cmd.bg())
    print("BGNOISE:" ,cmd.bgnoise())
    print("STAT:" ,cmd.stat())
    print("CDG:" ,cmd.cdg())
    print("ENTROPY:" ,cmd.entropy())
    
    app.Script("basic.ssf")
except Exception as e :
    print("\n**** ERROR *** " +  str(e) + "\n" )    

app.Close()
del app

print("StatusFunctions:end")
       
